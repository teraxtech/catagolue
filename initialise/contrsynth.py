
from urllib.request import urlopen, Request
from sys import argv
import os


def main():

    from shinjuku.search import read_components, comp_folder
    from shinjuku.gliderset import gset
    from shinjuku.transcode import encode_comp, decode_comp, realise_comp, remove_standard_spaceships
    from shinjuku.checks import rewind_check
    from shinjuku.synthtools import split_mosaic as split_synthesis

    def is_good(synthline):

        out_str = decode_comp(synthline)[2]
        pat = realise_comp(synthline)
        actual_str = pat.oscar(verbose=False, return_apgcode=True).get('apgcode', 'rubbish')
        return (out_str == actual_str)

    knowns = read_components()

    print("%d components known" % len(knowns))

    address = argv[2]

    # Download contrib.sjk:
    dl_address = address + "/textsamples/xs0_contrib/b3s23/synthesis"
    contrib_lines = urlopen(dl_address).read().decode()
    contrib_lines = [s for s in contrib_lines.split('\n') if '>' in s]

    # Download front of RLE queue
    dl_address = address + "/readsynth"
    rle_lines = urlopen(dl_address).read().decode()
    rle_lines = [s for s in rle_lines.split('\n\n') if 'x' in s]

    engulfed = set([])

    # Parse RLEs
    for rle in rle_lines:

        print(rle)
        for radius in [5, 8, 13, 21]:
            print("# Filtering with radius %d" % radius)
            try:
                subpatterns = [c for c in split_synthesis(rle, radius=radius-1, maxtime=1024)]
                for s in subpatterns:

                    w = s.wechsler
                    if (w != '#'):
                        if w in engulfed:
                            continue
                        print("# Engulfing %s" % w)
                        engulfed.add(w)

                    try:
                        dst = s[4096].apgcode
                        if s[4096][-4096] == s:
                            print("# Null component")
                            continue
                        x = remove_standard_spaceships(s)
                        src = (x - gset.extract(x).s()).apgcode
                        print("# processing src=%s, dst=%s..." % (src, dst))
                        c = encode_comp(s)
                        if rewind_check(*realise_comp(c, separate=True)):
                            contrib_lines.append(c)
                        else:
                            raise ValueError("Not rewindable.")
                        print("#     ...success.")
                    except (ValueError, KeyError, TypeError):
                        print("# Invalid component")
            except (ValueError, KeyError, TypeError):
                print("# Incomprehensible pattern!")

    # Deduplicate against repository:
    contrib_lines = [s for s in set(contrib_lines) if (s not in knowns) and ('ov_' not in s)]
    contrib_lines = [s for s in contrib_lines if is_good(s)]
    contrib_lines.sort()

    print("%d original contributions" % len(contrib_lines))

    # Save to file:
    if '.' in argv[1]:
        with open(argv[1], 'w') as f:
            for l in contrib_lines:
                f.write('%s\n' % l)
        return

    # Utilise existing synthesis-uploading framework:
    payload = "%s SYNTH b3s23\n" % argv[1]
    payload += "\n#CSYNTH xs0_contrib costs 0 gliders.\n"
    payload += '\n'.join(contrib_lines)
    payload += '\nThis line contains an exclamation mark!\n'

    req = Request(address + "/commonnames", payload.encode('utf-8'), {"Content-type": "text/plain"})
    f = urlopen(req)

    print(f.read())

    filename = os.path.join(comp_folder, 'cached_contrib.sjk')

    # Save contrib.sjk into components folder. This means that when we
    # subsequently run diffupdate.py (immediately after this script),
    # the user contributions will be included in Catagolue immediately,
    # instead of waiting until they are incorporated into the Shinjuku
    # repostory.
    with open(filename, 'w') as f:
        f.write('# Start of contrib.sjk\n')
        for x in contrib_lines:
            f.write('%s\n' % x)
        f.write('# End of contrib.sjk\n')

    print("Saved contributions into %s" % filename)

    to_delete = len(rle_lines) - 1

    if (to_delete >= 1):
        print("Deleting %d RLEs...\n" % to_delete)
        payload = "%s DELSYNTH %d\n" % (argv[1], to_delete)
        req = Request(address + "/commonnames", payload.encode('utf-8'), {"Content-type": "text/plain"})
        f = urlopen(req)

    print("The operation completed successfully.")

if __name__ == '__main__':

    main()
